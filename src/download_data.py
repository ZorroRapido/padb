from requests import get


def download():
    with open(file_name, "wb") as f:
        response = get(url)
        f.write(response.content)


if __name__ == '__main__':
    url = 'https://storage.yandexcloud.net/objects1/padb' \
          '/cian_parsing_result_sale_1_2_moskva_02_Jun_2023_21_02_18_116952.csv'
    file_name = '../data/raw/cian/' + url.split('/')[-1]
    download()
