"""House price prediction service"""

from flask import Flask, request

app = Flask(__name__)

config = {
    'model_path': 'models/model_1.joblib'
}

model = config['model_path']


@app.route("/")
def home():
    return 'Housing price service. Use /predict endpoint'


@app.route("/predict")
def predict():
    f1 = request.args.get("GrLivArea")
    f2 = request.args.get('f2')
    f3 = request.args.get('f3')
    f1, f2, f3 = int(f1), int(f2), int(f3)

    result = model.predict([[f1]])[0]
    return f'{round(result / 1000)} тыс. руб.'


if __name__ == '__main__':
    app.run(host='0.0.0.0')
